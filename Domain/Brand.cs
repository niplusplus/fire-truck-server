using System;
using System.Collections.Generic;

namespace Domain
{
    public class Brand
    {
        public Guid Id {get; set;}
        public string Name {get; set;}
        public string Code {get; set;}
        public ICollection<BuildUpBrand> BuildUpBrands {get; set;}
        public ICollection<DriveTransmissionBrand> DriveTransmissionBrands {get; set;}
        public ICollection<WheelDistanceBrand> WheelDistanceBrands {get; set;}
    }
}