using System;

namespace Domain
{
    public class WheelDistanceBrand
    {
        public Guid BrandId{get; set;}
        public Brand Brand{get; set;}
        public Guid WheelDistanceId{get; set;}
        public WheelDistance WheelDistance{get; set;}
    }
}