﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class BuildUp
    {
        public Guid Id {get; set;}
        public string Name {get; set;}
        public string Code {get; set;}
        public string LinkToGet {get; set;}
        public ICollection<BuildUpBrand> BuildUpBrands {get; set;}
    }
}
