using System;
using System.Collections.Generic;

namespace Domain
{
    public class DriveTransmission
    {
        public Guid Id {get; set;}
        public string Name {get; set;}
        public string Code {get; set;}
        public string LinkToGet {get; set;}
        public ICollection<DriveTransmissionBrand> DriveTransmissionBrands {get; set;}
    }
}