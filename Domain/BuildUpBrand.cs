using System;

namespace Domain
{
    public class BuildUpBrand
    {
        public Guid BrandId{get; set;}
        public Brand Brand{get; set;}
        public Guid BuildUpId{get; set;}
        public BuildUp BuildUp{get; set;}
    }
}