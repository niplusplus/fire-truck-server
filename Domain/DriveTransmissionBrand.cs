using System;

namespace Domain
{
    public class DriveTransmissionBrand
    {
        public Guid BrandId{get; set;}
        public Brand Brand{get; set;}
        public Guid DriveTransmissionId{get; set;}
        public DriveTransmission DriveTransmission{get; set;}
    }
}