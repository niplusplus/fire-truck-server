using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Brands
{
    public class Create
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var brand = new Brand{
                    Id = request.Id,
                    Name = request.Name,
                    Code = request.Code,
                };
                _context.Brands.Add(brand);
                var success = await _context.SaveChangesAsync() > 0;
                if(!success){
                    throw new Exception("Problem saving changes");
                }
                return Unit.Value;
            }
        }
    }
}