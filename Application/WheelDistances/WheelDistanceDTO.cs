using System;
using System.Collections.Generic;
using Application.Brands;

namespace Application.WheelDistances
{
    public class WheelDistanceDTO
    {
        public Guid Id {get; set;}
        public string Name {get; set;}
        public string Code {get; set;}
        public string LinkToGet {get; set;}
        public ICollection<BrandDTO> WheelDistanceBrands {get; set;}
    }
}