using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.WheelDistances
{
    public class Details
    {
        public class Query : IRequest<WheelDistanceDTO>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, WheelDistanceDTO>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;

            }

            public async Task<WheelDistanceDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                var wheelDistance = await _context.WheelDistances
                    .Include(x => x.WheelDistanceBrands)
                    .ThenInclude(x => x.Brand)
                    .SingleOrDefaultAsync(x => x.Id == request.Id);
                return _mapper.Map<WheelDistance, WheelDistanceDTO>(wheelDistance);
            }
        }
    }
}