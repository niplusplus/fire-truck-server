using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.WheelDistances
{
    public class Create
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string LinkToGet { get; set; }
            public string[] Brands {get; set;}
            public string[] BrandCodes {get; set;}
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var wheelDistance = new WheelDistance{
                    Id = request.Id,
                    Name = request.Name,
                    Code = request.Code, 
                    LinkToGet = request.LinkToGet
                };
                wheelDistance.WheelDistanceBrands = new List<WheelDistanceBrand>();
                for(int i = 0; i < request.Brands.Length; i++){
                    wheelDistance.WheelDistanceBrands.Add(new WheelDistanceBrand{
                        Brand = new Brand{
                            Name = request.Brands[i],
                            Code = request.BrandCodes[i]
                        }
                    });
                }
                _context.WheelDistances.Add(wheelDistance);
                var success = await _context.SaveChangesAsync() > 0;
                if(!success){
                    throw new Exception("Problem saving changes");
                }
                return Unit.Value;
            }
        }
    }
}