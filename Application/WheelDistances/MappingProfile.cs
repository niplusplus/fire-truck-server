using Application.Brands;
using AutoMapper;
using Domain;

namespace Application.WheelDistances
{
    public class MappingProfile: Profile
    {
        public MappingProfile(){
            CreateMap<WheelDistance, WheelDistanceDTO>();
            CreateMap<WheelDistanceBrand, BrandDTO>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Brand.Name))
                .ForMember(d => d.Code, o => o.MapFrom(s => s.Brand.Code));
        }
    }
}