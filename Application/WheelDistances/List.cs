using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.WheelDistances
{
    public class List
    {
        public class Query : IRequest<List<WheelDistanceDTO>> { }

        public class Handler : IRequestHandler<Query, List<WheelDistanceDTO>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }

            public async Task<List<WheelDistanceDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var wheelDistances = await _context.WheelDistances
                    .Include(x => x.WheelDistanceBrands)
                    .ThenInclude(x => x.Brand)
                    .ToListAsync();
                return _mapper.Map<List<WheelDistance>, List<WheelDistanceDTO>>(wheelDistances);
            }
        }
    }
}