using Application.Brands;
using AutoMapper;
using Domain;

namespace Application.DriveTransmissions
{
    public class MappingProfile: Profile
    {
        public MappingProfile(){
            CreateMap<DriveTransmission, DriveTransmissionDTO>();
            CreateMap<DriveTransmissionBrand, BrandDTO>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Brand.Name))
                .ForMember(d => d.Code, o => o.MapFrom(s => s.Brand.Code));
        }
    }
}