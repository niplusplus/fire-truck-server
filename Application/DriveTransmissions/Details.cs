using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.DriveTransmissions
{
    public class Details
    {
        public class Query : IRequest<DriveTransmissionDTO>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, DriveTransmissionDTO>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;

            }

            public async Task<DriveTransmissionDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                var driveTransmission = await _context.DriveTransmissions
                    .Include(x => x.DriveTransmissionBrands)
                    .ThenInclude(x => x.Brand)
                    .SingleOrDefaultAsync(x => x.Id == request.Id);

                return _mapper.Map<DriveTransmission, DriveTransmissionDTO>(driveTransmission);
            }
        }
    }
}