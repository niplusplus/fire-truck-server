using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.DriveTransmissions
{
    public class List
    {
        public class Query : IRequest<List<DriveTransmissionDTO>> { }

        public class Handler : IRequestHandler<Query, List<DriveTransmissionDTO>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }

            public async Task<List<DriveTransmissionDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var driveTransmissions = await _context.DriveTransmissions
                    .Include(x => x.DriveTransmissionBrands)
                    .ThenInclude(x => x.Brand)
                    .ToListAsync();
                return _mapper.Map<List<DriveTransmission>, List<DriveTransmissionDTO>>(driveTransmissions);
            }
        }
    }
}