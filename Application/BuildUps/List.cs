using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.BuildUps
{
    public class List
    {
        public class Query : IRequest<List<BuildUpDTO>> { }

        public class Handler : IRequestHandler<Query, List<BuildUpDTO>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }

            public async Task<List<BuildUpDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var buildUps = await _context.BuildUps
                    .Include(x => x.BuildUpBrands)
                    .ThenInclude(x => x.Brand)
                    .ToListAsync();

                return _mapper.Map<List<BuildUp>, List<BuildUpDTO>>(buildUps);
            }
        }
    }
}