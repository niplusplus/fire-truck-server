using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.BuildUps
{
    public class Details
    {
        public class Query : IRequest<BuildUpDTO>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, BuildUpDTO>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;

            }

            public async Task<BuildUpDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                var buildUp = await _context.BuildUps
                    .Include(x => x.BuildUpBrands)
                    .ThenInclude(x => x.Brand)
                    .SingleOrDefaultAsync(x => x.Id == request.Id);
                    
                return _mapper.Map<BuildUp, BuildUpDTO>(buildUp);
            }
        }
    }
}