using Application.Brands;
using AutoMapper;
using Domain;

namespace Application.BuildUps
{
    public class MappingProfile : Profile
    {
        public MappingProfile(){
            CreateMap<BuildUp, BuildUpDTO>();
            CreateMap<BuildUpBrand, BrandDTO>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Brand.Name))
                .ForMember(d => d.Code, o => o.MapFrom(s => s.Brand.Code));
        }
    }
}