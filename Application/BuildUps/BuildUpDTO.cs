using System;
using System.Collections.Generic;
using Application.Brands;

namespace Application.BuildUps
{
    public class BuildUpDTO
    {
        public Guid Id {get; set;}
        public string Name {get; set;}
        public string Code {get; set;}
        public string LinkToGet {get; set;}
        public ICollection<BrandDTO> Brands {get; set;}
    }
}