using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.BuildUps
{
    public class Create
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string LinkToGet {get; set;}
            public string[] Brands {get; set;}
            public string[] BrandCodes {get; set;}
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var buildUp = new BuildUp{
                    Id = request.Id,
                    Name = request.Name,
                    Code = request.Code,
                    LinkToGet = request.LinkToGet
                };
                buildUp.BuildUpBrands = new List<BuildUpBrand>();
                for(int i = 0; i < request.BrandCodes.Count(); i++){
                    buildUp.BuildUpBrands.Add(new BuildUpBrand{
                        Brand = new Brand{
                            Name = request.Brands[i],
                            Code = request.BrandCodes[i]
                        }
                    });
                }
                _context.BuildUps.Add(buildUp);
                var success = await _context.SaveChangesAsync() > 0;
                if(success){
                    return Unit.Value;
                }
                throw new Exception("Problem saving changes");
                
            }
        }
    }
}