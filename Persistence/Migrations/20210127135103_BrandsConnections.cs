﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class BrandsConnections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuildUpBrands",
                columns: table => new
                {
                    BrandId = table.Column<Guid>(nullable: false),
                    BuildUpId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildUpBrands", x => new { x.BuildUpId, x.BrandId });
                    table.ForeignKey(
                        name: "FK_BuildUpBrands_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildUpBrands_BuildUps_BuildUpId",
                        column: x => x.BuildUpId,
                        principalTable: "BuildUps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DriveTransmissionBrand",
                columns: table => new
                {
                    BrandId = table.Column<Guid>(nullable: false),
                    DriveTransmissionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriveTransmissionBrand", x => new { x.DriveTransmissionId, x.BrandId });
                    table.ForeignKey(
                        name: "FK_DriveTransmissionBrand_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriveTransmissionBrand_DriveTransmissions_DriveTransmissionId",
                        column: x => x.DriveTransmissionId,
                        principalTable: "DriveTransmissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WheelDistanceBrand",
                columns: table => new
                {
                    BrandId = table.Column<Guid>(nullable: false),
                    WheelDistanceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelDistanceBrand", x => new { x.WheelDistanceId, x.BrandId });
                    table.ForeignKey(
                        name: "FK_WheelDistanceBrand_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WheelDistanceBrand_WheelDistances_WheelDistanceId",
                        column: x => x.WheelDistanceId,
                        principalTable: "WheelDistances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildUpBrands_BrandId",
                table: "BuildUpBrands",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_DriveTransmissionBrand_BrandId",
                table: "DriveTransmissionBrand",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelDistanceBrand_BrandId",
                table: "WheelDistanceBrand",
                column: "BrandId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildUpBrands");

            migrationBuilder.DropTable(
                name: "DriveTransmissionBrand");

            migrationBuilder.DropTable(
                name: "WheelDistanceBrand");
        }
    }
}
