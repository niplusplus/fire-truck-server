﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Persistence;

namespace Persistence.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20210127135103_BrandsConnections")]
    partial class BrandsConnections
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.5");

            modelBuilder.Entity("Domain.Brand", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Code")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Brands");
                });

            modelBuilder.Entity("Domain.BuildUp", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Code")
                        .HasColumnType("TEXT");

                    b.Property<string>("LinkToGet")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("BuildUps");
                });

            modelBuilder.Entity("Domain.BuildUpBrand", b =>
                {
                    b.Property<Guid>("BuildUpId")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("BrandId")
                        .HasColumnType("TEXT");

                    b.HasKey("BuildUpId", "BrandId");

                    b.HasIndex("BrandId");

                    b.ToTable("BuildUpBrands");
                });

            modelBuilder.Entity("Domain.DriveTransmission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Code")
                        .HasColumnType("TEXT");

                    b.Property<string>("LinkToGet")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("DriveTransmissions");
                });

            modelBuilder.Entity("Domain.DriveTransmissionBrand", b =>
                {
                    b.Property<Guid>("DriveTransmissionId")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("BrandId")
                        .HasColumnType("TEXT");

                    b.HasKey("DriveTransmissionId", "BrandId");

                    b.HasIndex("BrandId");

                    b.ToTable("DriveTransmissionBrand");
                });

            modelBuilder.Entity("Domain.WheelDistance", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Code")
                        .HasColumnType("TEXT");

                    b.Property<string>("LinkToGet")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("WheelDistances");
                });

            modelBuilder.Entity("Domain.WheelDistanceBrand", b =>
                {
                    b.Property<Guid>("WheelDistanceId")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("BrandId")
                        .HasColumnType("TEXT");

                    b.HasKey("WheelDistanceId", "BrandId");

                    b.HasIndex("BrandId");

                    b.ToTable("WheelDistanceBrand");
                });

            modelBuilder.Entity("Domain.BuildUpBrand", b =>
                {
                    b.HasOne("Domain.Brand", "Brand")
                        .WithMany("BuildUpBrands")
                        .HasForeignKey("BrandId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.BuildUp", "BuildUp")
                        .WithMany("BuildUpBrands")
                        .HasForeignKey("BuildUpId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Domain.DriveTransmissionBrand", b =>
                {
                    b.HasOne("Domain.Brand", "Brand")
                        .WithMany("DriveTransmissionBrands")
                        .HasForeignKey("BrandId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.DriveTransmission", "DriveTransmission")
                        .WithMany("DriveTransmissionBrands")
                        .HasForeignKey("DriveTransmissionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Domain.WheelDistanceBrand", b =>
                {
                    b.HasOne("Domain.Brand", "Brand")
                        .WithMany("WheelDistanceBrands")
                        .HasForeignKey("BrandId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.WheelDistance", "WheelDistance")
                        .WithMany("WheelDistanceBrands")
                        .HasForeignKey("WheelDistanceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
