﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options){

        }

        public DbSet<Brand> Brands {get; set;}
        public DbSet<BuildUp> BuildUps {get; set;}
        public DbSet<DriveTransmission> DriveTransmissions {get; set;}
        public DbSet<WheelDistance> WheelDistances {get; set;}
        public DbSet<BuildUpBrand> BuildUpBrands {get; set;}

        protected override void OnModelCreating(ModelBuilder builder){
            base.OnModelCreating(builder);

            builder.Entity<BuildUpBrand>(x => x.HasKey(bb => 
                new {bb.BuildUpId, bb.BrandId}));

            builder.Entity<BuildUpBrand>()
                .HasOne(b => b.Brand)
                .WithMany(bb => bb.BuildUpBrands)
                .HasForeignKey(b => b.BrandId);

            builder.Entity<BuildUpBrand>()
                .HasOne(b => b.BuildUp)
                .WithMany(bb => bb.BuildUpBrands)
                .HasForeignKey(b => b.BuildUpId);

            builder.Entity<DriveTransmissionBrand>(x => x.HasKey(db => 
                new {db.DriveTransmissionId, db.BrandId}));
                
            builder.Entity<DriveTransmissionBrand>()
                .HasOne(b => b.Brand)
                .WithMany(db => db.DriveTransmissionBrands)
                .HasForeignKey(b => b.BrandId);

            builder.Entity<DriveTransmissionBrand>()
                .HasOne(d => d.DriveTransmission)
                .WithMany(db => db.DriveTransmissionBrands)
                .HasForeignKey(d => d.DriveTransmissionId);

            builder.Entity<WheelDistanceBrand>(x => x.HasKey(wb => 
                new {wb.WheelDistanceId, wb.BrandId}));
                
            builder.Entity<WheelDistanceBrand>()
                .HasOne(b => b.Brand)
                .WithMany(wb => wb.WheelDistanceBrands)
                .HasForeignKey(b => b.BrandId);

            builder.Entity<WheelDistanceBrand>()
                .HasOne(d => d.WheelDistance)
                .WithMany(wb => wb.WheelDistanceBrands)
                .HasForeignKey(d => d.WheelDistanceId);
        }
    }
}
